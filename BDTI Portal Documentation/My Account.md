# My Account
The *My Account* page contains two panels, the *User Info* and *DSL Info*. The *User Info* panel contains general information about the user (eg. username, email, groups). Some of the information presented here originates from the information the user entered upon their first login.

The second panel *DSL Info* contains information regarding the groups the user is assigned to. Specifically, it contains information regarding the usage quota for resources used in the group. Each deployment made by a user uses a certain amount of resources in terms of CPU, RAM, and storage; since there is a total quota for each group in terms of resources. Each group the user belongs to will have a table showing the usage of CPU, RAM, and storage across the group. The column called *Used* shows the total amount of resources by users, while the one called *Total* shows the total resource quota. To the right of that column is a progress bar illustration the usage percentage. Some resources also specify a limit per user. Note, if the user only sees the *DSL Info* panel then that user is not assigned to any group.

![Users and DSL info](<Material/User and DSL info/User__DSL_Info_.png>)

## Admin
The admin page is only available for admins. The admin will be able to assign or remove users from groups they belong to. The admin will see a table of all the users for each group the admin belongs to. To navigate through the list of users, the admin can use the search feature as well as the minimize feature (by clicking on the group it will minimize).

![Admin](<Material/admin/Admin.png>)

To add a user, press the Add button on the top right of the page. A popup will appear where the admin can specify the user they want to add and the group they will be added to. Note, the admin must use the user's username exactly, there is no auto suggestion.

![Add new user](<Material/Add New user/Add new user.png>)

To remove a user from a group the admin can click on the remove button next to the listed user they want to remove under the group they want to remove the user from. The admin will then be prompted if they are sure they want to remove the user.

