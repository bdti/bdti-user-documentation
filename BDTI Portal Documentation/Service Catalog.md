# Access to the service catalog
The service catalog page shows all of the available services and allows the user to launch new deployments based from one these services. To launch a new deployment, the user can press the Launch button on their desired service. This will open a popup where the user can specify the configurations. The popup has three initial fields which are present in all services: *Name*, *Group* and *Config*. *Name* specifies the name the deployment will take. The name can contain the following characters: a-z, A-Z, 0-9, _, -, however it must begin with a letter and finish with an alphanumeric. Each deployment must also be assigned to a group that the user belongs to. All three of these options are required.

![Jupyter](Material/jupyter/Jupyter_.png)

After filling the 3 basic fields another fiel will pop-up, named *Config*, which configures the resources allocated to each service. When configuring a service, a user can choose from the following configurations. The deployed service will book the related quota on the cluster node on which it is deployed:

##### Micro
- **CPU:** 500m (MilliCPUs)
- **Memory:** 500Mi (Mebibytes)

##### Small
- **CPU:** 1000m (MilliCPUs)
- **Memory:** 2000Mi (Mebibytes)

##### Medium
- **CPU:** 2000m (MilliCPUs)
- **Memory:** 4000Mi (Mebibytes)

##### Large
- **CPU:** 4000m (MilliCPUs)
- **Memory:** 8000Mi (Mebibytes)

##### Custom
The Custom configuration is available for all services, allowing users to specify a custom CPU and Memory allocation for the service they want to deploy. More information can be found in the following link: [Service Configuration](../BDTI%20Infrastructure%20Capabilities%20and%20Storage%20Soluction/Infrastructure%20capabilities%20and%20Storage%20solutions.md#service-configuration)

![Service Config](Material/service config/Service Config.png)

After choosing a config, the popup window will expand showing additional inputs. There are four types of possible inputs: a simple text field, a multi choice dropdown, a checkbox, or addable/removable key-value pairs.

Some of the fields cannot be edited by the user, these will have their input fields greyed out. Some fields are also mandatory, these will be labelled *Required* under the input field; the service cannot be launched if all these fields have not been filled out.


**Some services may ask for a username and/or password, these are credentials that are up to the user to choose, they are the credentials that will be used by the user to authenticate when accessing the deployment** 

Once the user has entered the required inputs, they can press the *Launch* button to launch the service. If the service has configured correctly then it will be deployed and a success message will appear, if the deployment fails an error message will be displayed; the user can view this new deployment in the *My Services* page.

![Jupyter part 2 ](<Material/Jupiter part 2/Jupyter__.PNG>)

The user can hover over the question mark symbol to see a description of the input field.

**Note:** A document  that covers information on the different ways that users can collaborate on the BDTI platform, with details on catalog service collaboration features and instructions on their usage, can be found here: [Collaborative Service Capabilities](/BDTI%20Collaborative%20Service%20Capabilities/Collaborative%20Service%20Capabilities.md)
