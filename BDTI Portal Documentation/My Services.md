#My Services
# My Services
## Deployments

The *Deployments* page contains a table with a list of deployments belonging to a user. The table has five columns which displays the deployment name, group, status, type, and creation date. The name and group are the respective names and group the user gave during deployment. The status of the deployment can either be *PENDING* or *ACTIVE*; when launching a new service it will be in the *PENDING* state, then once it is ready, it will go into the *ACTIVE* state. This process may take a few minutes. The type is the type of service launched, eg [Jupyter Lab](https://jupyter.org/) or [RStudio](https://www.rstudio.com/). Once the deployment goes into the *ACTIVE* status, the *Open/Copy* and *Terminate* buttons will appear; these buttons allow the user to open/copy or delete their deployment respectively.

There are two types of deployments, those with external and internal types of interfaces. Deployments with external interfaces are accessible from a web browser and therefore have an *Open* button, which once pressed will redirect the user to the deployment. Deployments with internal types of interfaces will be inside the private network and will therefore only be accessible from inside another deployment, they therefore have a *Copy* button which copies the deployments private IP to the clipboard.

![My Services Deployments](<Material/My Services Deployments/My Services Deployments.png>)

The user can also click on a deployment to display a popup with more detailed information about the specific deployment.

![Jupyter lab](<Material/jupyter lab/jupiter_lab.png>)

The user can press on the *Open* button to open the deployment and be redirected to a new tab with the deployed service. The user will then be able to log in using the credentials they provided during deployment.

![Jupyter Password](<Material/Jupyter Password/Jupyter Password .png>)

To delete a deployment the user can press on the *Terminate* button, then confirm deletion.

![Exit Jupyter Lab](<Material/Exit Jupyer lab/exitjupiter_lab.png>)