![BDTI Banner](<Material/img/BDTI_Banner_generic.png>)

# BDTI Portal Documentation
The BDTI portal is a web application which allows users to easily deploy and manage containerized data science workloads. In this  section you can find documentation about the portal.

The user documentation for the BDTI portal is divided into different sections:

- [Introduction](<Introduction.md>): An introduction to the portal and its uses, as well as a  brief overview of all the different pages in the portal.

- [My Account](<My Account.md>): Information about a user's account and group membership.

- [Service Catalog](<Service Catalog.md>) : Catalog of services  a user can launch on the BDTI portal.

- [My Services](<My Services.md>): Page where the user will find all their deployments.

- [User's Data](<User's data.md>):

    - [Secrets](<Secrets.md>): Secrets relating  to a user's deployments.

    - [Storage](<Storage.md>): Storage accounts that a user has access to.