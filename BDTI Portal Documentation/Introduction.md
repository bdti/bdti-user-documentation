# Introduction to the BDTI Portal

In the following section, you will find an introduction to the portal and its uses, as well as a brief overview of all the different pages in the portal.

## Connection to the BDTI Portal

To access the BDTI Portal, the users needed to have an EU login account.

- In case, users did not already have an  [EU Login account](https://ecas.ec.europa.eu/cas/login?loginRequestId=ECAS_LR-3393217-YSlTczI22DF1CIzfxDzUcEMRspHutQdaRFPpXXPlgaKMLkMNxkcXMSJR5j8JDS18rkhJF2VD9fpv0mV7hXaFTPW-jpJZscgsw0K3vD6SDJEi6O-eKaoI6ekJamg8whEi0FCmTt6yeRHzfjhoaoIu1n7bbmze2k3NrfvAb4I2ezkD5e6tzdu8zameqhkDmQ01jPt905),they should create one using [create an account link](https://ecas.ec.europa.eu/cas/eim/external/register.cgi). For more information, one can visit the [Help page](https://ecas.ec.europa.eu/cas/eim/external/help.cgi).
- If the user already has an EU Login account, should send the following information to EC-PILOT-BDTI:

  - Unique identifier at the Commission (uid)
  
  - Associated email

Please note that these information can be found after the creation of the [EU Login](https://ecas.ec.europa.eu/cas/login?loginRequestId=ECAS_LR-8642467-XgdaYyhMRyYlqSiSE9uQJzGzY5AUZzjMmjookqrpvb9ldlXKktg00Nl3C2WRbgYgh1ye1wyg7r1Lqg68ZAg8zzzm-yntOf97TTHq0GemtNMIM6i-HNiEXd0HZpv6mzeGucMepPJYPwHplnQlT8FgQLx7GraGhnBTgoNuLYRobcmYdYSRBLpfzLb3aVKo0lRBzSp9kM0) in 'My Account' under ['My account details](https://ecas.ec.europa.eu/cas/userdata/myAccount.cgi)'.


![My Account](<Material/my account/My_Account.PNG>)

## What is a Deployment

The portal is a web application which allows users to easily deploy and manage containerized data science workloads. These containerized workloads are referred to as deployments; these deployments are individual instances of their respective services. These services can be application like [Jupyter Lab](https://jupyter.org/) or [RStudio](https://www.rstudio.com/), or database tools such as [MongoDB](https://www.mongodb.com/), or [PostgreSQL](https://www.postgresql.org/).

![Deployment](<Material/Deployment/Deployment.png>)

Users have access to a service catalog in the portal of all the available services, from where they are be able to deploy new instances of these services. When launching these deployments, users are able to specify certain configuration options such as CPU and RAM allocation or the crendentials they want to use to access the deployments. After launching a deployment, users will be able to access it through a web browser where they can use it like any other web based application. Once the user no longer needs the deployment, they can terminate it without fear of losing data since these deployments are attached to persistent storage.

Note, the database deployments cannot be accessed through a browser, instead they can be access from inside another deployment.

## Types of Users

There are two different types of users, regular users and admins. Admins have all the rights a regular user has, with the addition of being able to manage groups. The admin page is only available to admins. The admin will be able to remove users from groups they belong to. The admin will see a table of all the users for each group the admin belongs to. To navigate through the list of users, the admin can use the search feature as well as the minimize feature (by clicking on the group it will minimize).

![Types of users](<Material/Types of users/Types of Users.png>)

To add a user, press the Add button on the top right of the page. A popup will appear where the admin can specify the user they want to add and the group they will be added to. Note, the admin must use the user's username exactly, there is no auto suggestion.

## Portal Overview

 Once the user is logged in they will see the full the portal. It is comprised of two main elements, the sidebar and the pages themselves. The Sidebar allows the users to navigate to different pages in the portal by clicking on their respective buttons. Each page will have it's content in panels. Some of the pages have subpages, for example the *My Account* and *Admin* pages has the *Monitoring* subpage. Note that this subpage will only appear when the user has navigated to their respective parent pages. If the user is an administrator they will see the *Admin* tab in the sidebar, if the user is a regular user they will not see this tab. To logout the user simply need to press the Logout button on the bottom left of the screen..

### Homepage

This is default page for the portal; it contains general information pertinent to all users.

![welcome portal](<Material/welcome portal/Welcome_page.png>)

### My Account

The *My Account* page contains two panels, the *User Info* and *DSL Info*. The User Info panel contains general information about the user (eg. username, email, groups). The second panel *DSL Info* contains information regarding usage quotes for the groups the user is assigned to.

![User info](<Material/User and DSL info/User__DSL_Info_.png>)


### Service Catalog

The *Service Catalog* page contains a single panel which lists all the possible services a user can deploy. From this page the user will be able to deploy new instance of these services.

![service catalog](<Material/service catalog/Service Catalog.jpg>) 


### My Services

The *My Services* page contains a single panel with a table showing all the deployments belonging to the user. From here the user is able to open/terminate existing deployments.

![My Services Deployments](<Material/My Services Deployments/My Services Deployments.jpg>)

### My Data

The *My Data* page contains two panels [*Secrets*](Secrets.md) and [*Storage*](Storage.md) which pertain to the user's secrets and storage respectively. The first panel *Secrets* allows the user to manage their secrets kept in the vault. The second panel *Storage* lists all the storage which can be used to store/access the user's data inside their deployments.

![Mt Data](<Material/My Data/My Data.png>)

### Admin
The *Admin* page can only be seen by admin users, it has one panel, where admins can add or remove users from groups.

![Admin](<Material/admin/Admin.png>)
