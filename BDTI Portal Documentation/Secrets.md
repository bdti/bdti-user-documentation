# Secrets

Secrets are key-value pairs which are stored in the vault, the key represents the name of the secret and the value represents the value of the secret. They fall under one of two categories *Private* and Shared. Private secrets can only be seen by the user who creates them, while *Shared* secrets are shared with all the members of a certain group.

**Please only store secrets related to your deployments eg. tokens.**

## How to create a Secret 
To create a new secret, first navigate to the *My Data* page, there the user will see the Secrets panel. Press the *Create* button.

![Create a secret](<Material/Create a Secret/secret.png>)

A popup will appear to create a new secret, the user will have 3 inputs to determine. The *Name* and *Value* represents the name and value of the secret respectively. *Visibility* determines whether the secret should be private or shared, for shared secrets the user must choose a group to share the secret with. The user can toggle the view/hide button to show or hide the secret.

![Create Secret part 2 ](<Material/Create Secret part 2/Create Secret part 2 .png>)

Once the user has filled out the 3 inputs, press the *Create* button to create the secret.

![Create Secret part 3 ](<Material/Create  Secret part 3/Create new secret part 3.png>)

## Viewing a secret 
The user can see all of their secrets, both private and those shared in a group they belong to in a table. This table show the secret's name, whether it is private or shared, and the group it belongs to if it is shared. There is also a reload button on the top right of the table, which allows the user to reload the secrets list.

![Viewing a secret](<Material/Viewing a secret/Viewing a secret.png>)

To view the secret's value, the user can click on the secret. By default the secret is hidden, however by pressing the view/hide button, the user can view or hide the secret

![Viewing a secret part 2](<Material/Viewing a secret part 2/Viewing a secret part 2.png>)

## Updating a secret 
To update a secret, the user must press on the edit button first, they will then be able to edit the secret's value. Once the secret has been edited, the user can press the *Update* button. **Important Note: This feature is currently not working properly and should be omitted**

![Updating a Secret](<Material/Updating a secret/Updating a Secret.png>)

## Deleting a secret 
To delete a secret, the user simply needs to press the *Delete* button.

![Delete a Secret](<Material/Deleting a Secret/Deleting a Secret.png>)
