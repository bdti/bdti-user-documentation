# Storage 
 
## This section provides you information about storage on the BDTI portal. Storage solutions are used to store a user's data.

The storage panel contains a table of all the storage the user has access to. The leftmost column shows the name of the storage options, the second column shows the group the storage options belongs to, and finally the third columns shows the type of storage options. There are 3 types of storage options, [EFS](https://aws.amazon.com/efs/) (Elastic File System), AWS [S3](https://aws.amazon.com/s3/) bucket, and [MinIO](https://min.io/), along with the default EBS storages allocated to running services.

The [EFS](https://aws.amazon.com/efs/) storage is the persistent network file system disk option which is used by the deployments to store user data. [MinIO](https://min.io/) is an object storage solution and is an option for the user to login and use [S3](https://aws.amazon.com/s3/) compatible storage. The user can create a bucket and then manage it either through the CLI or the UI. [S3](https://aws.amazon.com/s3/) is an object storage solution to store a variety of data types and formats and can be used as ingestion layer or data lake for example. The [MinIO](https://min.io/) storage options can be opened, while the other types cannot.

![Storage](<Material/storage/Storage.png>)

More information can be found here: [Storage Solutions](<../BDTI Infrastructure Capabilities and Storage Solutions/Infrastructure capabilities and Storage solutions.md#storage-solutions>)
