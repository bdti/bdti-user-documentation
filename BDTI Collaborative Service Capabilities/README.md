![BDTI Banner](<Material/img/BDTI_Banner_generic.png>)

#  BDTI Collaborative Service Capabilities

Find guidance on the different ways that users can collaborate on the BDTI platform, with details on catalog service collaboration features and instructions on their usage.

The information is generated as of the latest release of the platform.
[Collaborative Service Capabilities](<Collaborative Service Capabilities.md>)