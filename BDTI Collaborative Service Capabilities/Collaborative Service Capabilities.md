# Collaborative Service Capabilities Documentation

This document covers information on the different ways that users can collaborate on the BDTI platform, with details on catalog service collaboration features and instructions on their usage.

The information is generated as of the latest release of the platform.

## Table of Contents
1. [Service Capabilities Matrix](#service-capabilities-matrix)
2. [Instructions](#instructions)
   - [User Management (per service)](#user-management-per-service)
     - [Airflow v2.7.1](#airflow---v271)
     - [Apache Superset v2.1](#apache-superset---v21)
     - [Doccano - v1.8.4](#doccano---v184)
     - [ElasticSearch v8.5.1 and Kibana v8.5.1](#elasticsearch---v851--and-kibana---v851)
     - [Mage AI v0.9.55](#mage-ai---0955)
     - [Metabase v0.47.1](#metabase---v0471)
     - [MinIO RELEASE-2023-07-07T07-13-57Z](#minio---release2023-07-07t07-13-57z)
     - [PgAdmin4 v7.6 and Postgresql v15.4.0](#pgadmin4---v76--and-postgresql---v1540)
     - [RStudio v4.4.0](#rstudio---v440)
     - [Virtuoso v7.2.10](#virtuoso---v7210)
     - [QGIS - v3.10.4](#qgis---v3104)
   - [Code Collaboration](#code-collaboration)
     - [Jupyterlab lab-4.0.4 all-spark-notebook and Jupyterlab lab-4.1.8 datascience-notebook](#jupyterlab---lab-404---all-spark-notebook--and-jupyterlab---lab-418---datascience-notebook)
     - [Mage AI v0.9.55](#mage-ai---0955-1)
     - [RStudio v4.4.0](#rstudio---v440-1)
     - [VSCode-Python - v2.6.0 & VSCode-R - v2.6.0](#vscode-python---v260--vscode-r---v260)     


## Service Capabilities Matrix

The following table provides an overview of collaborative features offered by various services within the platform.

| Feature | Description |
| ------- | ----------- |
| **Multi-user** | Indicates whether a service can be simultaneously accessed and used by multiple team members. This is essential for scenarios where several developers or collaborators need to work on the same project or dataset. <br> **Note:** Services are designed for single-user access by default. However, access to the service can be shared with multiple users by specifying the service as 'Shared' during deployment. |
| **VCS** | Denotes if a service offers integration with a Version Control System (like Git) for code management. VCS integration enables historical tracking of code changes, facilitates branching and merging for parallel development, and provides a safety net if you need to revert to previous versions. |
| **Data persistence** | Specifies if a service supports persistence storage using Network File System (NFS). This allows users to save their work (datasets, configurations, model files, etc.) within their dedicated user folders. This data can then be accessed from other services that also support NFS, ensuring data portability and continuity within workflows. NFS provisioning may be done on a per-DSL/group basis. |
| **Web console** | Indicates if a service provides a web-based interface (app or console), offering a user-friendly way to manage the service's settings, functionality, and status without requiring direct command-line interaction. |
| **Public access** | Specifies whether a service can be directly accessed from a public endpoint (outside the platform). Services without public access are generally only accessible from within the platform's internal network. |


| Service                                 | Category        | Data persistence | Web console | Public access | Multi-user | VCS |
|-----------------------------------------|-----------------|------------------|-------------|---------------|------------|-----|
| **Airflow - v2.7.1**                        | Orchestration   | X                | X           | X             | X          |     |
| **Apache Superset - v2.1**                  | Visualization   |                  | X           | X             | X          |     |
| **Doccano - v1.8.4**                        | Processing, IDE |                  | X           | X             | X          |     |
| **ElasticSearch - v8.5.1**                  | Processing      |                  |             |               |            |     |
| **H2o-3 - v42.0.4**                         | IDE             | X                | X           |               | X          |     |
| **Jupyterlab - lab-4.0.4 - all-spark-notebook** | IDE         | X                | X           | X             |            | X   |
| **Jupyterlab - lab-4.1.8 - datascience-notebook** | IDE     | X                | X           | X             |            | X   |
| **Kibana - v8.5.1**                         | Visualization   |                  | X           | X             | X          |     |
| **Knime - 5.1.0**                           | IDE             | X                | X           | X             |            |     |
| **Knime - 5.2.1**                           | IDE             | X                | X           | X             |            |     |
| **Mage AI - 0.9.55**                        | IDE, Orchestration | X            | X           | X             | X          | X   |
| **Metabase - v0.47.1**                      | Visualization   |                  | X           | X             | X          |     |
| **MinIO - RELEASE.2023-07-07T07-13-57Z**    | Storage         | X                | X           | X             |            |     |
| **MongoDB - v6.0.9**                        | Storage         |                  |             |               |            |     |
| **PgAdmin4 - v7.6**                         | Visualization   |                  | X           | X             | X          |     |
| **PostgreSQL - v15.4.0**                    | Storage         |                  |             |               |            | X   |
| **RStudio - v4.4.0**                        | IDE             | X                | X           | X             | X          | X   |
| **Spark - v3.4.1**                          | Processing      | X                | X           |               |            |     |
| **Virtuoso - v7.2.10**                      | Storage         | X                | X           | X             |            |     |
| **QGIS - v3.10.4**                          | IDE             | X                | X           | X             |            |     |
| **VSCode-Python- v2.6.0**                   | IDE             | X                | X           | X             |            | X   |
| **VSCode-R- v2.6.0**                        | IDE             | X                | X           | X             |            | X   |


## Instructions

### User Management (per service)
**Disclaimer:** Users can launch services with "SHARED" as the sharing status. This option makes the deployment and its initial secret(s) visible and manageable by the rest of the DSL users.

Using single-user services by multiple users (at the same time) might not work as expected and therefore it is recommended to:
- Use each service's management system in case of multi-user services
- Deploy individual services for each user
- Communicate with DSL users through a common channel before using the shared service

Using single-user services by multiple users (at the same time) might produce the following side-effects:
- Possibility for user session issues and app data state being corrupted
- User data will only persist in the initial workload/service owner's unique folder. If other users access another user's workload, in case of a service that uses an NFS disk (see Data persistence column), generated data are not persisted to each individual user's unique NFS mounted folder but only to the service owner's mounted folder.

#### Airflow - v2.7.1
**Creating New Airflow Users**
1. Access the Airflow UI: Navigate to your Airflow web interface.
2. Go to Security: Locate the "Security" tab or section in the UI.
3. List of Users: Click on "List of Users" to see existing accounts.
4. Create User: Find the "+" button to create a new user.
5. Fill in Details: Enter the new user's required details.
6. Select Roles: Assign appropriate roles (e.g., Admin, User, Viewer) to determine the user's permissions.
7. Save: Click "Save" to create the new user account.

**Managing Airflow Users**
1. Security Tab: Return to the "Security" tab or section.
2. List of Users: View a list of all existing users.
3. Edit User: Click on a username to modify their details or roles.
4. Delete User: If necessary, select the option to delete a user account.

<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![Airflow Users](Material/Airflow/agc-airflow-users.png)


</details>

#### Apache Superset - v2.1
**Creating New Superset Users**
1. Access Superset UI: Navigate to your Superset web interface.
2. Security Menu: Find the "Settings" menu on the top right.
3. List Users: Click on "List Users" to see existing accounts.
4. Add User: Click the "+" button to add a user.
5. Fill in Details: Enter the new user's required details.
6. Assign Roles: Select one or more roles to establish the user's access levels and permissions.
7. Save: Click "Save" to finalize the new user account.

**Managing Superset Users**
1. Security Menu: Return to the "Security" menu.
2. List Users: View a list of all existing users.
3. Edit User: Click on a username to modify details like first name, last name, email, and to change their roles/permissions.
4. Delete User: If necessary, use the options to delete a user.

<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![Apache Superset Users](Material/Apache%20Superset/image-2024-4-29_14-27-46.png)

</details>

#### Doccano - v1.8.4
**Creating New Doccano Users**
1. Access the Doccano UI /admin/login path of your deployed instance
2. Login with your admin credentials
3. Make sure to navigate to the Doccano UI /admin path
4. Click on the "Add" button to create a new user
5. Set username and password and click "Save"
6. Once more familiar with permissions, create a Group and assign members instead of direct permissions assignment

**Managing Superset Users**
1. Access the Doccano UI /admin/login path of your deployed instance
2. Login with your admin credentials
3. Make sure to navigate to the Doccano UI /admin path
4. Click on the "Users" button to see all the users
5. Click on a user and configure its details (groups, permissions, name etc)

<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![Doccano Users 1](Material/Doccano/doccano-users-1.png)
  ![Doccano Users 2](Material/Doccano/doccano-users-2.png)
  ![Doccano Users 3](Material/Doccano/doccano-users-3.png)
  ![Doccano Users 4](Material/Doccano/doccano-users-4.png)
  ![Doccano Users 5](Material/Doccano/doccano-users-5.png)

</details>

#### ElasticSearch - v8.5.1 & Kibana - v8.5.1
**Creating New ElasticSearch Users**
1. Access Kibana UI: Navigate to your Kibana web interface.
2. Go to Stack Management: Go to "Stack Management" from the left pane.
3. Manage Users: Select "Users" under the "Security" section.
4. Create User: Click "Create User" and provide username and password.
5. Assign Roles: Select from built-in roles or create custom ones on the "Roles" tab.
6. Save: Click the "Create user" button.

**Managing ElasticSearch Users**
1. Security Menu: Return to the "Users" menu.
2. List Users: View a list of all existing users.
3. Edit User: Click on a username to modify details like first name, last name, email, and to change their roles/permissions.
4. Delete User: If necessary, use the options to delete a user.

<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![Elastic Search](Material/ElasticSearch/image-2024-4-29_14-27-46.png)

</details>

#### Mage AI - 0.9.55
**Creating New Users in Mage AI**
1. Access Settings: Navigate to your Mage project settings.
2. Users Tab: Click on the "Users" navigation row.
3. Add User: Click the "Add new user" button.
4. User Details: Enter the user's required details.

**Managing Users in Mage AI**
1. Settings > Users: Return to the project settings and "Users" tab.
2. View User List: See existing users and their roles.
3. Edit Roles: As an owner, click on a user to modify their assigned roles.
4. Reset Password: Help users change their passwords if needed.


<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![Mage AI](Material/Mage%20AI/image-2024-4-29_14-28-42.png)

</details>

#### Metabase - v0.47.1
**Creating New Users in Metabase**
1. Admin Settings: Access the gear icon (top right corner) and select "Admin Settings".
2. People Tab: Navigate to the "People" tab.
3. Add Person: Click the "Invite Someone" button.
4. User Details: Enter the new user's name and email address.
5. Choose Groups: Assign the user to one or more groups to determine their data access permissions.
6. Send Invite: Click "Create" to create user and obtain the temporary user password.

**Managing Metabase Users**
1. People Tab: Return to the "People" menu.
2. List Users: View a list of all existing users.
3. Edit User: Click on a user to modify its details.
4. Delete User: If necessary, use the options to delete a user.

<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![Metabase](Material/Metabase/image-2024-4-29_14-28-48.png)

</details>

#### MinIO - RELEASE.2023-07-07T07-13-57Z
**Creating Users in MinIO UI**
1. Access MinIO UI: Navigate to your MinIO web interface.
2. IAM Section: Find the "Identity" section in the UI.
3. Users Tab: Click on the "Users" tab.
4. Create User: Click on the "Create User +" button.
5. User Details: Enter the new user's required details.
6. Choose policy: Select an existing user policy or create a custom one and assign it.

**Managing Minio Users**
1. Users Tab: Return to the "Users" menu.
2. View User List: See all existing users.
3. Edit User: Click on the pencil button to modify a user's details.
4. Delete User: Select a user and use the "Delete selected" button to remove it.

<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![MinIO](Material/MinIO/image-2024-4-29_14-28-55.png)

</details>

#### PgAdmin4 - v7.6 & Postgresql - v15.4.0
**Creating PostgreSQL Users in pgAdmin 4**
1. Connect to Server: In pgAdmin 4, connect to the PostgreSQL server where you'll create the user.
2. Database Properties: Right-click on the desired database and select "Properties."
3. Security Tab: Navigate to the "Security" tab.
4. Create Login/Group Role: Click on the "+" button or use the "Create Login/Group Role" option.
5. Role Name: Enter a name for the new database user.
6. Definition Tab: Set the user's password.
7. Privileges Tab: Grant specific database permissions (Can Login, Create Databases, etc.).
8. Save: Click "Save" to create the new user.

**Managing PostgreSQL Users in pgAdmin 4**
1. Database Properties > Security: Return to the security settings of the database.
2. View User List: See all existing database users (roles).
3. Edit User: Click on a user to modify their password, privileges, and other settings.
4. Delete User: Select a user and use the delete option to remove the role from the database.

<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![PostgreSQL](Material/Postgre/agc-pgadmin-users.png)

</details>

#### RStudio - v4.4.0
**Enabling RStudio Multi-User Capability**
1. Choose RStudio and click "Launch."
2. In Group and Config, select "Enable Multi-User" and your NFS PVC.
3. RStudio will deploy with 6 total users (rstudio + rstudio1-rstudio5). Find credentials in the "My Data" tab.

**File System and Persistence**
- Data resides under /home/rstudio (EFS mount point) with subdirectories for each user.
- Users have a /shared folder within their home directories (linked to /home/rstudio/shared) for file sharing.
- EFS ensures data persistence for all users across deployments.

<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![RStudio](Material/RStudio/rstudio-multiuser.PNG)
  ![RStudio](Material/RStudio/rstudio-multiuser2.PNG)

</details>

#### Virtuoso - v7.2.10
**Creating additional Virtuoso Users**
1. Access Conductor: Go to your Virtuoso Conductor interface by selecting the "Conductor" tab and logging in using the "dba" user credentials.
2. System Admin Tab: Navigate to the "System Admin" tab.
3. User Accounts: Select the "User Accounts" tab.
4. Create User: Click the "Create Account" button.
5. Enter the username and password.
6. Optionally, assign roles (DBA, SPARQL, etc.).

**Manage Users**
- Edit, delete, or change passwords of existing users in the "User Accounts" tab.

<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![Virtuoso](Material/Virtuoso/Virtuoso1.PNG)
  ![Virtuoso2](Material/Virtuoso/Virtuoso2.PNG)
  ![Virtuoso3](Material/Virtuoso/Virtuoso3.PNG)

</details>

#### QGIS - v3.10.5
**Creating additional QGIS users**
- QGIS Desktop: Log in Desktop and run QGIS
- Settings: Go to the "Settings" tab
- Profile: Go to "User Profiles" and click "New Profile"


<details>
  <summary>Click to expand visual instructions...</summary>
  
  ![QGIS](Material/QGIS/QGISusers1.PNG)
  ![QGIS2](Material/QGIS/QGISusers2.PNG)
  ![QGIS3](Material/QGIS/QGISusers3.PNG)
  ![QGIS4](Material/QGIS/QGISusers4.PNG)

</details>

## Code Collaboration

#### Jupyterlab - lab-4.0.4 - all-spark-notebook & Jupyterlab - lab-4.1.8 - datascience-notebook
To start managing your project with Git, navigate to the Git tab located on the left-hand panel of your interface. Within this tab, locate and open the Git extension to access the full range of Git functionalities.

  ![JupyterLab](Material/JupyterLab/jupyterlab-git.gif)

#### Mage AI - 0.9.55
Mage AI provides several methods for integrating Git into your workflow. 
Consult the [official documentation](https://docs.mage.ai/development/git/configure) to determine the optimal approach for your development process.

#### RStudio - v4.4.0
To manage projects using Git, first ensure you are logged in. Then, open a terminal window using the keyboard shortcut ALT+SHIFT+R. 
  ![JupyterLab](Material/RStudio%20Code%20Colab/2024-04-29%2012-41-18.png)

This terminal provides an interface where you can input Git commands to interact with your code repositories.

#### VSCode-Python - v2.6.0 & VSCode-R - v2.6.0
For code collaboration on VSCode using Git, follow these steps:

1. Navigate to the Git tab on the left-hand side of your interface.
2. You will need to login and authorize device access, then you can simply start managing your project with Git.
3. Open the Git extension within this tab to access the full range of Git functionalities.

  ![VSCode](Material/VS Code/Github Authorized OAuth Apps.png)

A similar Github [Authorized OAuth Apps page](https://github.com/settings/applications) should be visible once the device is successfully authorized. 
Then you can navigate and manage your project as the following:

  ![VSCode2](Material/VS Code/Github Pull Request.png)
  ![VSCode3](Material/VS Code/Github Create Pull Request.png)
