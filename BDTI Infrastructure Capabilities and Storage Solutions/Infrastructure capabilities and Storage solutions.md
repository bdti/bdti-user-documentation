# BDTI Infrastructure capabilities and Storage solutions

## Table of Contents

- [BDTI Infrastructure capabilities and Storage solutions](#bdti-infrastructure-capabilities-and-storage-solutions)
  - [Table of Contents](#table-of-contents)
  - [Services Offered by the Platform](#services-offered-by-the-platform)
    - [Databases](#databases)
    - [Data Lake](#data-lake)
    - [Development Environments](#development-environments)
    - [Advanced Processing](#advanced-processing)
    - [Visualization](#visualization)
    - [Orchestration](#orchestration)
  - [Service Configuration](#service-configuration)
  - [Resource Quotas](#resource-quotas)
    - [Summary of Resource Quotas](#summary-of-resource-quotas)
  - [Storage Solutions](#storage-solutions)
    - [Performances and Limitations of EBS and EFS Storage Solutions](#performances-and-limitations-of-ebs-and-efs-storage-solutions)
      - [EBS](#ebs)
      - [EFS](#efs)
    - [Storage Solutions by Service](#storage-solutions-by-service)

## Services Offered by the Platform

The BDTI platform is a web application which allows users to easily deploy and manage containerized data science workloads from the ones available in the Service Catalogue :

### Databases

- **PostgreSQL**: A relational database available to store data in a predefined structured format for querying and analysis. PostGIS Extension was added in the latest version.
- **MongoDB**: A document-oriented database for storing JSON-like documents with optional schemas. It is a distributed database designed for high availability, horizontal scaling, and geographic distribution.
- **Virtuoso**: A graph database for collecting data of every type and using hyperlinks as super-keys to create a change-sensitive and conceptually flexible web of linked data.

### Data Lake

- **MinIO**: A high-performance Kubernetes native object storage for storing unstructured data such as photos, videos, log files, and backups.

### Development Environments

- **JupyterLab**: A web-based interactive development environment for Jupyter notebooks, code, and data.
- **RStudio**: An Integrated Development Environment (IDE) for R, a programming language for statistical computing and graphics.
- **KNIME**: An open source data analytics, reporting, and integration platform for machine learning and data mining throughout the data science life cycle.
- **H2O.ai**: An open source machine learning and AI platform designed to simplify and accelerate ML and AI operations in any environment.
- **QGIS**: QGIS is an open-source Geographic Information System that enables users to create, edit, visualize, analyze, and publish geospatial information on Windows, Mac, and Linux platforms.
- **VSCode - Python**: The VSCode-Python extension enhances Visual Studio Code with robust support for Python development, providing features such as IntelliSense, debugging, linting, Jupyter Notebook integration, and more.
- **VSCode - R**: The VSCode-R extension enhances Visual Studio Code with robust support for R programming, offering features like syntax highlighting, debugging, linting, R Markdown integration, and more.
- **Doccano**: Doccano is an open-source annotation tool for text data, designed to facilitate the creation of labeled datasets for natural language processing (NLP) tasks. It provides a web-based interface for annotating text.

### Advanced Processing

- **Apache Spark**: Used for implementing Spark clusters for distributed processing of large volumes of data.
- **Elasticsearch**: A distributed search and analytics engine for real-time search across various use cases like logs analysis and spatial information management.
- **Kibana**: A browser-based analytics and search dashboard for Elasticsearch.

### Visualization

- **Apache Superset**: An open source modern data exploration and visualization platform able to handle big data at petabyte scale.
- **Metabase**: A tool that connects to your database and brings its data to life in beautiful visualizations with an intuitive interface.

### Orchestration

- **Apache Airflow**: An open source workflow management platform for scheduling and running complex data pipelines.
- **Mage AI**: An open-source tool designed as a modern alternative to Airflow for building, running, and managing data pipelines in Python, SQL, or R.

More details for each service can be found in the following link: [Service Offering](<https://big-data-test-infrastructure.ec.europa.eu/service-offering_en>)

## Service Configuration

When configuring a service, users can choose from the following configurations, which determine the quota on the cluster node:

- **Micro**
  - CPU: 500m (MilliCPUs)
  - Memory: 500Mi (Mebibytes)
- **Small**
  - CPU: 1000m (MilliCPUs)
  - Memory: 2000Mi (Mebibytes)
- **Medium**
  - CPU: 2000m (MilliCPUs)
  - Memory: 4000Mi (Mebibytes)
- **Large**
  - CPU: 4000m (MilliCPUs)
  - Memory: 8000Mi (Mebibytes)
- **Custom**: Users can specify a custom CPU and Memory allocation.
  - CPU: up to 7910m (MilliCPUs)
  - Memory: up to 29.92Gi (Gibibytes)

The cluster nodes are Amazon EC2 m5.2xlarge machines characterized by:

- vCPUs: 8
- Memory (GiB): 32
- Storage: EBS Only
- Network Bandwidth (Gbps): Up to 10
- EBS Bandwidth (Gbps): Up to 4.750

**Note 1**: As defined in the **Custom Service Configuration** Not all machine capacity can be consumed by user-deployed services. Resources are reserved for supporting mandatory services to ensure correct operation. Users can check their resource quota via My Account > DSL Info.

**Note 2**: The maximum allocatable vCPU for a service, is the **catalog** type node-group machine type max available CPU units and the assigned group limit (whichever is lower). If the available node is an [m5.2xlarge](https://aws.amazon.com/ec2/instance-types/m5/), the upper CPU limit is a little less than 8 vCores (8000 m) due to system resource reservation which is about **7910 m**. It is recommended to use less than the limit to ensure a successful deployment and provide space to potential one-time initialization jobs.

![CPU](<./Material/CPU/CPU.png>)

**Note 3**: The maximum allocatable memory for a service, is the **catalog** type node-group machine type max available memory units and the assigned group limit (whichever is lower). If the available node is an [m5.2xlarge](https://aws.amazon.com/ec2/instance-types/m5/), the upper memory limit is a little less than 32 Gi (32768 Mi) due to system resource reservation which is about **29.92 Gi**. It is recommended to use less than the limit to ensure a successful deployment and provide space to potential one-time initialization jobs.

![RAM](<./Material/RAM/RAM.png>)

**Note 4**: The configuration for the services of Airflow, Apache Superset and Spark - V3.4.1 are slight different and more resources are kept for each default configuration profile.

More specifically:
- **Airflow:**
  - Small: 
  vCPUs: 4.75
  Memory (GiB): 7
  - Medium: 
  vCPUs: 6.75
  Memory (GiB): 11
  - Large: Currently not Supported

- **Apache Superset:**
  - Small: 
  vCPUs: 3.25
  Memory (GiB): 5.5
  - Medium: 
  vCPUs: 5.25
  Memory (GiB): 9.5
  - Large:
  vCPUs: 7.25
  Memory (GiB): 13.5

- **Spark:**
  - Small: 
  vCPUs: 6
  Memory (GiB): 6
  - Medium: 
  vCPUs: 10
  Memory (GiB): 10
  - Large: 
  vCPUs: 14
  Memory (GiB): 12



## Resource Quotas

The BDTI platform, a multi-tenant EKS cluster, requires standardized default quotas for new pilot groups to ensure fair and efficient resource allocation. These quotas define the limits on resources such as CPU, memory, and storage.

- **CPU Quota**
  - Description: The maximum amount of CPU resources allocated to each pilot.  
  - Value: 15 vCPUs

- **Memory Quota**
  - Description: The maximum amount of memory allocated to each pilot.  
  - Value: 30 GB

- **Storage Quota**
  - Description: The maximum amount of persistent storage allocated to each pilot group (soft limit, can be overcommitted).  
  - Value: up to 1 TB

### Summary of Resource Quotas

- **CPU Quota**: 15 vCPUs
- **Memory Quota**: 30 GB
- **Storage Quota**: up to 1 TB

#### With this setup, pilot users could run

- 1 Custom Service with maximum resources allocated (7.91 vCPU, 29.92GB RAM)

**Or for example:**

- 3 x Large Service (12 vCPU , 24GB RAM)
- 1 x Medium Service (2 vCPU, 4GB RAM)
- 1 x Small Service (1 vCPU, 2GB RAM)

More information about the Service Configuration can be found in the previous section: [Service Configuration](#service-configuration).

##### Implementation Details

- **Namespaces**: Each pilot group will be assigned a dedicated namespace within the EKS cluster to ensure isolation and manageability.
- **Quota Enforcement**: Quotas will be enforced using Kubernetes ResourceQuotas and LimitRanges to ensure that no pilot group exceeds their allocated resources.
- **Adjustments**: Quotas can be adjusted based on pilot group needs and feedback, subject to approval by the Official.

## Storage Solutions

Services are backed by either Amazon Elastic File System (EFS) or Amazon EBS General Purpose Volumes (gp3). The following tables map the storage solutions for each service:

### Performances and Limitations of EBS and EFS Storage Solutions

#### EBS

| **Storage Type** | **IOPS**                | **Throughput** | **Latency**                  |
|------------------|-------------------------|----------------|------------------------------|
| **EBS (gp3)**    | 260,000                 | 12,500 MB/s    | Single digit millisecond     |

#### EFS

| File System Type | Throughput Mode               | Latency - Read Operations              | Latency - Write Operations             | Maximum IOPS - Read Operations   | Maximum IOPS - Write Operations | Maximum throughput - Per-File-System Read                                    | Maximum throughput - Per-File-System Write | Maximum throughput - Per-Client Read/Write                          |
|------------------|-------------------------------|------------------------------|------------------------------|-------------------|------------------|--------------------------------------------------------|------------------------|-----------------------------------------------|
| Regional         | Elastic                       | As low as 250 microseconds (µs) | As low as 2.7 milliseconds (ms) | 90,000–250,000    | 50,000           | 3–20 gibibytes per second (GiBps)                       | 1–5 GiBps              | 1,500 mebibytes per second (MiBps)            |
| Regional         | Provisioned                   | As low as 250 µs              | As low as 2.7 ms              | 55,000            | 25,000           | 3–10 GiBps                                            | 1–3.33 GiBps           | 500 MiBps                                    |
| Regional         | Bursting                      | As low as 250 µs              | As low as 2.7 ms              | 35,000            | 7,000            | 3–5 GiBps                                             | 1–3 GiBps              | 500 MiBps                                    |
| One Zone         | Elastic, Provisioned, Bursting| As low as 250 µs              | As low as 1.6 ms              | 35,000            | 7,000            | 3 GiBps4                                              | 1 GiBps4               | 500 MiBps                                    |

**Footnotes:**

1. Maximum read and write throughput depend on the AWS Region. Throughput in excess of an AWS Region's maximum throughput requires a throughput quota increase. Any request for additional throughput is considered on a case-by-case basis by the Amazon EFS service team. Approval might depend on your type of workload.
2. File systems that use Elastic throughput can drive a maximum of 90,000 read IOPS for infrequently accessed data and 250,000 read IOPS for frequently accessed data.
3. The maximum combined read and write throughput is 1,500 MiBps.

Our BDTI platform implementation uses:

- Regional type
- Elastic throughput mode

For BDTI, related EFS disk storage option, the following limits are valid:

| File System Type | Throughput Mode               | Latency - Read Operations              | Latency - Write Operations             | Maximum IOPS - Read Operations   | Maximum IOPS - Write Operations | Maximum throughput - Per-File-System Read                                    | Maximum throughput - Per-File-System Write | Maximum throughput - Per-Client Read/Write                          |
|------------------|-------------------------------|------------------------------|------------------------------|-------------------|------------------|--------------------------------------------------------|------------------------|-----------------------------------------------|
| Regional         | Elastic                       | As low as 250 microseconds (µs) | As low as 2.7 milliseconds (ms) | 90,000–250,000    | 50,000           | 3–20 gibibytes per second (GiBps)                       | 1–5 GiBps              | 1,500 mebibytes per second (MiBps)            |


**Please note that the processing of stored data within the BDTI infrastructure is highly dependent on the specific use case of each Pilot.** 
Factors such as the type of data, the service utilized, and various other dimensions play a critical role in determining the appropriate processing methods. Therefore, it is essential to evaluate and tailor the data processing strategy according to the unique requirements of each individual pilot.

### Storage Solutions by Service

| **Service**         | **Storage** |
|---------------------|-------------|
| PostgreSQL          | EBS         |
| PgAdmin             | EBS         |
| MongoDB             | EBS         |
| Virtuoso            | EBS         |
| MinIO               | EBS         |
| JupyterLab          | EFS         |
| RStudio             | EFS         |
| KNIME               | EFS         |
| H2O.ai              | -           |
| Apache Spark        | -           |
| Elasticsearch       | EBS         |
| Kibana              | -           |
| Apache Superset     | EBS         |
| Metabase            | -           |
| Apache Airflow      | EBS, EFS    |
| Mage AI             | EFS         |
| Doccano             | EBS
| VSCode              | EFS         |
| QGIS                | EFS         |