![BDTI Banner](<Material/img/BDTI_Banner_generic.png>)

# BDTI Infrastructure Capabilities and Storage Solution

Find a dedicated source for the main capabilities offered by the BDTI platform, in terms of the services offered and their configuration, along with the storage solutions that could be provided:
[Infrastructure capabilities and Storage solutions](<Infrastructure capabilities and Storage solutions.md>).