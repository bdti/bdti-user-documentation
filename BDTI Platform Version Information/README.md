![BDTI Banner](<Material/img/BDTI_Banner_generic.png>)

# BDTI Platform Version Information

Welcome to the dedicated source for all updates and news regarding the BDTI platform. Our platform is continually evolving to meet the needs of our users, and this page serves as your official resource for all the latest information.

## Current Version

Stay up-to-date with the latest release of the BDTI platform. Here, you'll find comprehensive details about the most recent version, including performance improvements and security enhancements that keep your operations smooth and secure.

**Current version:** 
[Version: 2.0.0 (19.07.2024)](<Version_2.0.0_19.07.2024.md>)
In the BDTI Platform's version 2.0.0 release on July 19, 2024, significant code enhancements were implemented to improve user experience and analytical capabilities improvements included the introduction of a more intuitive user interface, making the platform easier to navigate and use additionally, new GIS analytics options were added, leveraging services like QGIS and the GIS extension in PostgreSQL, thereby expanding the platform's analytical functionalities updates reflect BDTI's commitment to providing a user-friendly and feature-rich environment for its users.

**Past version:** 
[Version: 1.1.0 (10.04.2024)](<Version_1.1.0_10.04.2024.md>)
In the BDTI Platform's version 1.1.0 release on April 10, 2024, significant code enhancements were implemented to bolster data security and improve user experience improvements included the introduction of advanced data encryption methods, ensuring more robust protection of sensitive information additionally, the platform's codebase was optimized to enhance performance and responsiveness, resulting in a more seamless and efficient user experience updates reflect BDTI's commitment to maintaining a secure and user-friendly environment for its users.

**Past version:**
[Version: 1.0.0 (15.12.2023)](<Version_1.0.0_15.12.2023.md>)
The BDTI Platform's version 1.0.0, released on December 15, 2023, introduced a user-friendly interface, integration with popular updated data processing tools, and basic data security measures to protect user information.



Here is a summarised table  of the improvements made in **BDTI Platform Version 1.0.0 (December 15, 2023)**, **BDTI Platform Version 1.1.0 (April 10, 2024)** and **Version 2.0.0 (July 19, 2024)** across different categories.

| **Category**       | **Version 1.0.0 (15.12.2023)** | **Version 1.1.0 (10.04.2024)** | **Version 2.0.0 (19.07.2024)** |
|--------------------|--------------------------------|--------------------------------|--------------------------------|
| **Security**       | Basic data security measures implemented. | Enhanced data encryption for improved protection. | Continued security enhancements with optimized access control measures. |
| **Performance**    | Initial platform implementation with core functionalities. | Code optimizations to enhance platform responsiveness and efficiency. | Further performance refinements for faster data processing. |
| **User Experience** | User-friendly interface introduced. | General improvements to interface usability. | Introduction of a more intuitive and user-friendly interface. |
| **Analytics & Data Processing** | Basic data processing tools integrated. | Strengthened backend processing for data accuracy. | Added GIS analytics features using QGIS and PostgreSQL GIS extension. |
| **Functionality**  | Core system functionalities implemented. | Improved system stability and bug fixes. | Expanded features for data visualization and geospatial analysis. |
