# Version: 2.0.0 (19.07.2024)

In the BDTI Platform's version 2.0.0 release on July 19, 2024, significant code enhancements were implemented to improve user experience and analytical capabilities improvements included the introduction of a more intuitive user interface, making the platform easier to navigate and use additionally, new GIS analytics options were added, leveraging services like QGIS and the GIS extension in PostgreSQL, thereby expanding the platform's analytical functionalities updates reflect BDTI's commitment to providing a user-friendly and feature-rich environment for its users.

## What's New

Discover the latest features and services introduced in the BDTI platform. Our commitment to innovation ensures you have access to cutting-edge tools that enhance your workflow and productivity:

- **New Catalog Services:**
  - QGIS
  - VSCode-Python
  - VSCode-R
  - Docanno
  - General introduction of Helm .tpl for all services
- **Portal Frontend Features:**
  - Persistence of catalog service username type fields
  - Admins' ability to manage all private deployments within assigned groups
- **New Documentation Pages:**
  - [Collaborative Service Capabilities](<../BDTI Collaborative Service Capabilities/Collaborative Service Capabilities.md>)
  - [Infrastructure Capabilities and Storage Solutions](<../BDTI Infrastructure Capabilities and Storage Solutions/Infrastructure capabilities and Storage solutions.md>)
  - Catalog-Services-Config-Extraction
- **Infrastructure Enhancements:**
  - KMS encryption at rest & transit via AWS Managed key or AWS CMK
  - KMS encryption for K8s Secrets, S3 buckets, EC2 Launch Templates, Lambda, EBS K8s StorageClass, EKS CloudWatch Log Groups, EFS, RDS DBs
  - KMS encryption for Application-layer (Keycloak, Vault, OpenFaaS Gateway)
  - KMS encryption for Support-layer services (Velero, ELK, Prometheus-Grafana, Kubecost)
  - S3 bucket versioning and logging
  - S3 portal bucket Cloudtrail logging and HTTPs restriction
  - Introduction of K8s NetworkPolicies and PodSecurityStandards
  - New chart for policy mapping generation
  - ECR images moved to private registry with IAM permissions
  - WAF on CloudFront for portal frontend
- **Support-Layer Enhancements:**
  - Prometheus-Grafana Thanos metrics export to S3
  - Prometheus-Grafana PushGateway ingestion of AWS EFS disk size metrics via K8s CronJobs using IRSA
- **OpenFaaS Backend Improvements:**
  - Logging library (winston) and logging statements
  - Helm configuration export to database during service launching (includes sensitive value masking)
  - Extra catalog service Helm labels & annotations during service deployment
- **Docker Enhancements:**
  - Image for EFS CW metrics extraction to Prometheus PushGateway
  - Image for Prometheus PushGateway metrics cleanup
  - Dockerignore file to avoid unnecessary files


## Changes and Enhancements

Learn about the recent modifications and enhancements. Whether it's a minor tweak or a major overhaul, you'll find detailed descriptions of what has changed and how these improvements contribute to a better user experience.

- **Catalog Service Updates:**
  - **Postgresql image updated with new extension (PostGIS)**
  - Jupyterlab allspark image updated with new tools
  - Jupyterlab datascience-notebook image updated with new tools
  - Rstudio image updated with new tools
- **Infrastructure Upgrades:**
  - **EKS K8s version upgrade** from 1.26 to 1.27 and **1.28**
  - Parts to ensure usage of AWS IMDSv2
  - Default block storage volumes upgraded to AWS gp3
  - EBS & EFS drivers updated
  - Disabled usage of subnet public IP auto-assignment
  - Layer charts version bump
- **Documentation Updates:**
  - Catalog-Services
  - KMS encryption documentation
- **Application Layer Enhancements:**
  - Charts version bump
- **Support Layer Enhancements:**
  - Charts version bump
- **OpenFaaS Backend Improvements:**
  - Full code revamp (structure, comments, unit-tests, code improvements, error-handling, logging, updated dependencies and libraries, nodejs watchdog template)
  - SSL with AWS certificate for RDS DB connections
  - Improved function deployment script
  - New database SQL schema (Deployment history table)
- **Portal Frontend Improvements:**
  - Dependencies upgrade
  - Disabled sourcemap generation
  - Documentation update
- **Docker Improvements:**
  - job-init-db now uses the new KMS-encrypted K8s StorageClass as the default in all catalog services

## Fixed Issues

- **Portal Frontend:**
  - Service details modal rendering
  - User relog prompt once session expires
- **Catalog Service:**
  - Jupyterlab spark libraries & tools compatibility fixed
- **Infrastructure:**
  - WAF rules on ALB to avoid portal frontend false positive failures
  - Automatic deletion of K8s PVCs with a deletion policy in StatefulSets (Airflow case)
- **Support Layer:**
  - ELK automation and configuration of index template, lifecycle policy, and sharding to avoid losing logs
- **OpenFaaS Backend:**
  - Resource calculation bug fixed
- **Security:**
  - Several security vulnerabilities in docker images fixed

## Recently Removed Services

To streamline our offerings and improve overall service quality, certain features and services have been discontinued. This section provides a summary of what has been removed and how these changes might affect your usage of the platform:

- **General Cleanup:**
  - Unnecessary files
  - Commented code
  - Outdated documentation references to dedicated wiki