# Version: 1.0.0 (15.12.2023) 

The BDTI Platform's version 1.0.0, released on December 15, 2023, introduced a user-friendly interface, integration with popular updated data processing tools, and basic data security measures to protect user information. 

## What's New

Discover the latest features and services introduced in the BDTI platform. Our commitment to innovation ensures you have access to cutting-edge tools that enhance your workflow and productivity:

- Frontend - New 'Deployment ID' field in service list modal.
- openfaas-init-db - AWS deployment and Docker ECR push functionalities.
- Updated Apache Airflow version, v1.11.0.
- Updated Apache Superset version, v2.1.
- Updated ElasticSearch version, v8.5.1.
- Updated H2o-3	version, v42.0.4.
- Updated Jupyterlab (all-spark-notebook) version, v4.0.4.
- Updated Jupyterlab version, v4.0.4.
- Updated Kibana version, v8.5.1.
- Updated Knime version, v5.1.0.
- Updated Metabase version, v0.47.1.
- Updated MinIO version, RELEASE.2023-07-07T07-13-57Z.
- Updated MongoDB version, v6.0.9.
- Updated PgAdmin4 version, v7.6.
- Updated Postgresql version, v15.4.0.
- Updated RStudio version, v4.3.1.
- Updated Spark version, v3.4.1.
- Updated Virtuoso version, v7.2.10.

## Recently Removed Services

To streamline our offerings and improve overall service quality, certain features and services have been discontinued. This section provides a summary of what has been removed and how these changes might affect your usage of the platform:

- openfaas-init-db - Removed the flag disabling the deploy step.

## Changes and Enhancements

Learn about the recent modifications and enhancements. Whether it's a minor tweak or a major overhaul, you'll find detailed descriptions of what has changed and how these improvements contribute to a better user experience.

- EKS-Infrastructure-Layer - Enhanced security with optimized AWS WAF rules and refined policies.
- Frontend - Updated the CSS to support the new field in the modal.
- Catalog Services - Upgraded Helm charts across all services to align with the latest service/application versions. 
- openfaas-init-db - Updated pipeline configuration.
- Helm Chart - Adjustments for v1.0.0.
- openfaas-init-db - Resolved ECR login and repo target issues, fixed JSON errors in index.js, and script corrections including quotes handling in PSQL JSON columns.