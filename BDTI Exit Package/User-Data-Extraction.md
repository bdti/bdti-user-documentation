# User data extraction

## Table of contents
- [User data extraction](#user-data-extraction)
  - [Table of contents](#table-of-contents)
  - [Downloading data persisted in network attached storage](#downloading-data-persisted-in-network-attached-storage)
    - [Jupyterlab](#jupyterlab)
    - [Rstudio](#rstudio)
    - [Knime](#knime)
  - [Downloading data persisted in block storage volumes](#downloading-data-persisted-in-block-storage-volumes)
    - [Doccano](#doccano)
    - [Postgresql](#postgresql)
    - [PgAdmin4](#pgadmin4)
    - [Superset](#superset)
  - [Push data to an S3 bucket](#push-data-to-an-s3-bucket)
    - [1.Workloads' data from an IDE service:](#1workloads-data-from-an-ide-service)
    - [2.Other services:](#2other-services)


_**Disclaimer:**_ The instructions and processes documented on this page are to be intended for informative purposes only and are provided on a best-effort base by System Owners. Thus, this documentation is on progress and could change over time.

## Downloading data persisted in network attached storage

A typical Data Science Lab group, contains a network attached storage which can be used in certain catalog services. To identify which services can attach a network storage volume, look for the `NFS PVC name` input in the launch dialog.

Through such services, user data can be persisted in the NFS volume under individual user folder of the user launching the service. This way, the user can persist and carry data around different NFS compatible services.

Below you can find ways to access and extract those data.

### Jupyterlab

Once you deploy a Jupyterlab instance you can **download** your data using the Jupyter file explorer user interface.

![jupyterlab-download-file](Material/15ce2c1d8906060c5d290d8f6d3ee5a0/jupyterlab-download-file.jpeg)

Since Jupyterlab allows **only single file download**, you can tar/zip certain folders to simplify the extraction process either via terminal or programmatically.

- Terminal: `tar -czf <tarfile-name>.tgz <folder-path>`
- Python code via Interactive Python Notebook

```python
import shutil

shutil.make_archive('<zipfile-name>', 'zip', 'folder-path')
```

![jupyterlab-data-extraction](Material/f215e3a46c37cb54585d4478bd990a1b/jupyterlab-data-extraction.jpeg)

### Rstudio

In RStudio we can **download** a folder by selecting the `Export` option in the file explorer section. But we can similarly compress files for easier downloads.

![rstudio-download-file](Material/539fabd4b1cbafb56d054508d8e5f96e/rstudio-download-file.jpeg)

- Terminal: `tar -czf <tarfile-name>.tgz <folder-path>`
- R code

```r
files2zip <- dir('<folder-path>', full.names = TRUE)
zip(zipfile = '<zipfile-path>', files = files2zip)
```

![rstudio-zip-file](Material/5b6a97466405a105b774bd8883a13217/rstudio-zip-file.jpeg)

### Knime

To use Knime, we connect to the instance through noVNC, a remote access app. This app does not provide file-transfer. Therefore, we can **export** `Knime project files` to the attached NFS volume and download them through another service such as RStudio or Jupyterlab.

![knime-export](Material/26a2ad7a179b320840aebe18ef4b8f36/knime-export.JPG)

## Downloading data persisted in block storage volumes

Most non IDE catalog services use block storage volumes. In that case, the user might not have direct access to the file system. Data from such services are exported programmatically or through interfaces that the each service allows.

For such cases, it is recommended to check possible export functions or persistent storage options such as databases (e.g Postgresql, MongoDB) and blob storages (e.g S3, Minio), as the data will not persist once the workloads are terminated.

### Doccano
For Doccano project data, we have to export the projects' content in order to be able to re-import them later or in another Doccano instance. There is no single project export. Once we have a project with a dataset and labels we can export those using the "Export" actions for dataset and labels:

![doccano-export-1](Material/Doccano/doccano-project-export-1.jpg)
![doccano-export-2](Material/Doccano/doccano-project-export-2.jpg)

Now that we have the data we can create a new project and import them:

![doccano-import-0](Material/Doccano/doccano-project-import-0.jpg)
![doccano-import-1](Material/Doccano/doccano-project-import-1.jpg)
![doccano-import-2](Material/Doccano/doccano-project-import-2.jpg)
![doccano-import-3](Material/Doccano/doccano-project-import-3.jpg)
![doccano-import-4](Material/Doccano/doccano-project-import-4.jpg)


### Postgresql

For Postgresql database data, we will need to connect to it through an interface such as PgAdmin4. See the instructions below.

### PgAdmin4

Once logged in, we can manage different relational databases. Data can be **uploaded** or **downloaded** in the PgAdmin4 instance through the usage of `Storage Manager` (under "Tools"). Within the `Preferences` menu (under "File"), the upload size can be specified as the default might be too low. Having the data in Storage Manager, a user can **import** or **backup/extract** database data.

![pgadmin-backup-data](Material/5dc4706e24734440c6ecc7acdc20bc65/pgadmin-backup-data.jpeg)

![pgadmin-storage-manager](Material/0d265628d97d827a29289dd04c00b664/pgadmin-storage-manager.jpeg)

![pgadmin-download-file](Material/fddcb135c7198d3d03cadc4c8a50022f/pgadmin-download-file.jpeg)

### Superset

In Superset you can export `Database Connections`, `Datasets`, `Charts` and `Dashboards`. To do that effectively you can include all of your charts within a dashboard and export that dashboard to include most of it automatically instead of exporting them individually.

![superset-export-db-con](Material/58b1d603fa872410dbd3f43ba79803a9/superset-export-db-con.jpeg)

![superset-export-datasets](Material/76214c92100e671b854ade0c58ed2749/superset-export-datasets.jpeg)

![superset-export-charts](Material/1a454b4fd4d05d020ffea60a2e8f1a06/superset-export-charts.jpeg)

![superset-export-dashboard](Material/fa322c81c2905b2124df3dfcd3462cb4/superset-export-dashboard.jpeg)

## Push data to an S3 bucket
When saving your specific workloads' data to an S3 bucket, there are two possible scenarios:

1. **Workloads' data from an IDE service** (_i.e. RStudio, JupyterLab_): those are the data that are generated 
2. **Other services**: for those services, users must download data to local storage (as showed in the previous access) and then push it to S3 via programmatic access.

### 1.Workloads' data from an IDE service:
In the case of **JupyterLab**, users can leverage the following code to upload a file on the target AWS S3 bucket:

[Upload_files_to_S3.ipynb](Material/ab13d7889239f34daa86f7331b4fd9ba/Upload_files_to_S3.ipynb)

### 2.Other services:
In case of services other than IDEs, users can download their data on local storage and then push it to the target S3 bucket by leveraging the AWS CLI. This can be accomplished by following the steps:

1. Install the AWS CLI by following the [official AWS Documentation](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
2. To upload files using the AWS CLI, execute the following command:
`aws s3 cp /path/to/source s3://bucket-name/ --recursive`
3. Make sure to replace the `path/to/source` with your local directory and `bucket-name` with the S3 bucket name provided by ECDP SysOps. The `--recursive` flag ensures all files and subdirectories are uploaded.
