![BDTI Banner](<Material/img/BDTI_Banner_generic.png>)

#  BDTI Exit Package

Find guidance on extracting or backing up your data from the platform in the document linked below. 
[User Data Extraction Guide](<User-Data-Extraction.md>)