# Explanation of Pilots Cost Report 

As participants in the BDTI platform, it's important to understand the various components that contribute to the overall cost of the platform you are utilizing. These costs are directly related to the resources consumed and services provided to ensure optimal performance and reliability of your applications. The main cost components are:

## CPU (Central Processing Unit)

- **Description**: The CPU is the brain of the computer where most calculations take place. In cloud environments, CPU resources are provisioned as virtual CPUs (vCPUs) or compute units.
- **Cost Implication**: Costs are incurred based on the number of CPU cores allocated and the duration of their usage. Allocating more CPU resources can improve application performance but will increase costs proportionally.

## RAM (Random Access Memory)

- **Description**: RAM is the system's short-term data storage, storing information that your applications are actively using for quick access.
- **Cost Implication**: Costs are calculated based on the amount of memory allocated. Adequate RAM ensures smooth operation and efficiency but requires balancing to avoid unnecessary expenses.

## Persistent Volume

- **Description**: Persistent Volumes provide durable storage that retains data even when applications are stopped or restarted. This is essential for databases and applications requiring data persistence.
- **Cost Implication**: Costs are associated with the amount of storage space provisioned. Factors such as storage type (e.g., SSD vs. HDD) and data redundancy features may also affect the cost.

## Load Balancer

- **Description**: A Load Balancer distributes incoming network traffic across multiple servers to ensure no single server becomes a bottleneck, enhancing application availability and reliability.
- **Cost Implication**: Costs are based on the number of load balancers used, the amount of data processed, and the duration of usage. Utilizing load balancers is crucial for high-traffic applications but does add to the overall cost.

## Shared Resources

- **Description**: Shared resources refer to infrastructure components like networking equipment, shared storage systems, or common services that are utilized by multiple applications.
- **Cost Implication**: The costs for shared resources are distributed based on usage metrics or a predefined allocation method.

## Other Cluster Costs

Beyond the resources directly consumed by your applications, there are additional cluster-wide costs associated with essential services that ensure the Kubernetes environment operates smoothly and securely. These include **backup** solutions like Velero, **ingress controllers** such as NGINX, **monitoring** and **logging systems**, core Kubernetes components in the `kube-system` namespace (e.g., kube-proxy), **storage interface drivers** for EFS and EBS, **logging stacks** like ELK, **cost analysis tools**, and resources that may be idle or unmounted. While these services are vital for the overall functionality, management, and reliability of the cluster, they consume compute and storage resources, contributing to the total operational costs. It's important to account for these shared expenses when assessing your overall cost structure, as they are distributed across all users and applications within the cluster.

## Additional Cloud Costs

In addition to the expenses detailed in the Kubecost report—which focuses on Kubernetes cluster resources—there are other cloud costs to consider. These include charges for the **core components of the BDTI platform**, such as the Kubernetes **master nodes** that manage cluster operations, and services like the **RDS (Relational Database Service) database** used for data storage outside the cluster. Other costs may involve data transfer and bandwidth usage, as well as fees for external services that support your applications but are not part of the Kubernetes environment. Accounting for these additional expenses provides a more complete understanding of your overall cloud spending.