![BDTI Banner](<img/BDTI_Banner_generic.png>)

# BDTI User Documentation

Welcome, user.

This section provides you with technical documentation pertinent to the BDTI platform.


## BDTI Portal Documentation

The BDTI portal is a web application which allows users to easily deploy and manage containerized data science workloads. In this  section you can find documentation about the portal.

The user documentation for the BDTI portal is divided into different sections:

- [Introduction](<BDTI Portal Documentation/Introduction.md>): An introduction to the portal and its uses, as well as a  brief overview of all the different pages in the portal.

- [My Account](<BDTI Portal Documentation/My Account.md>): Information about a user's account and group membership.

- [Service Catalog](<BDTI Portal Documentation/Service Catalog.md>) : Catalog of services  a user can launch on the BDTI portal.

- [My Services](<BDTI Portal Documentation/My Services.md>): Page where the user will find all their deployments.

- [User's Data](<BDTI Portal Documentation/User's data.md>):

    - [Secrets](<BDTI Portal Documentation/Secrets.md>): Secrets relating  to a user's deployments.

    - [Storage](<BDTI Portal Documentation/Storage.md>): Storage accounts that a user has access to.

## BDTI Infrastructure Capabilities and Storage Solution
Find a dedicated source for the main capabilities offered by the BDTI platform, in terms of the services offered and their configuration, along with the storage solutions that could be provided:
[Infrastructure capabilities and Storage solutions](<BDTI Infrastructure Capabilities and Storage Solutions/Infrastructure capabilities and Storage solutions.md>).

##  BDTI Collaborative Service Capabilities

Find guidance on the different ways that users can collaborate on the BDTI platform, with details on catalog service collaboration features and instructions on their usage.

The information is generated as of the latest release of the platform
[Collaborative Service Capabilities](<BDTI Collaborative Service Capabilities/Collaborative Service Capabilities.md>).

## BDTI Platform Version Information

Find a dedicated source for all updates and news regarding the BDTI platform. Our platform is continually evolving to meet the needs of our users, and this page serves as your official resource for all the latest information. More information can be found in the respective page: [BDTI Platform Version Information](<BDTI Platform Version Information>).

**Current version:** 
[Version: 2.0.0 (19.07.2024)](<BDTI Platform Version Information/Version_2.0.0_19.07.2024.md>)

**Past version:** 
[Version: 1.1.0 (10.04.2024)](<BDTI Platform Version Information/Version_1.1.0_10.04.2024.md>)

## BDTI Exit Package

Find guidance on extracting or backing up your data from the platform in the document linked below
[User Data Extraction Guide](<BDTI Exit Package/User-Data-Extraction.md>).